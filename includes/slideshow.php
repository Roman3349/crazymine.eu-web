<div class="slideshow">
    <div id="slideshow-wrap">
        <input type="radio" id="button-1" name="controls" checked>
        <label for="button-1"></label>
        <input type="radio" id="button-2" name="controls">
        <label for="button-2"></label>
        <input type="radio" id="button-3" name="controls">
        <label for="button-3"></label>
        <input type="radio" id="button-4" name="controls">
        <label for="button-4"></label>
        <input type="radio" id="button-5" name="controls">
        <label for="button-5"></label>
        <label for="button-1" class="arrows" id="arrow-1">>></label>
        <label for="button-2" class="arrows" id="arrow-2">>></label>
        <label for="button-3" class="arrows" id="arrow-3">>></label>
        <label for="button-4" class="arrows" id="arrow-4">>></label>
        <label for="button-5" class="arrows" id="arrow-5">>></label>
        <div id="slideshow-inner">
            <ul>
                <li id="slide1">
                    <img src="http://images5.alphacoders.com/296/296147.jpg" alt="">
                    <div class="description">
                        <input type="checkbox" id="show-description-1">
                        <label for="show-description-1" class="show-description-label">I</label>
                        <div class="description-text">
                            <h2>Nadpis</h2>
                            <p>Text</p>
                        </div>
                    </div>
                </li>
                <li id="slide2">
                    <img src="http://images6.alphacoders.com/380/380205.jpg" alt="" />
                    <div class="description">
                        <input type="checkbox" id="show-description-2"/>
                        <label for="show-description-2" class="show-description-label">I</label>
                        <div class="description-text">
                            <h2>Nadpis</h2>
                            <p>Text</p>
                        </div>
                    </div>
                </li>
                <li id="slide3">
                    <img src="http://zonehdwallpapers.com/wp-content/uploads/2013/02/Herobrine-Island-Minecraft-HD-Wallpaper.jpg" alt="" />
                    <div class="description">
                        <input type="checkbox" id="show-description-3"/>
                        <label for="show-description-3" class="show-description-label">I</label>
                        <div class="description-text">
                            <h2>Nadpis</h2>
                            <p>Text</p>
                        </div>
                    </div>
                </li>
                <li id="slide4">
                    <img src="http://cocoabelle.com/wp-content/uploads/Minecraft-Volcano-HD-Wallpaper-For-Desktop.jpg" alt="" />
                    <div class="description">
                        <input type="checkbox" id="show-description-4"/>
                        <label for="show-description-4" class="show-description-label">I</label>
                        <div class="description-text">
                            <h2>Nadpis</h2>
                            <p>Text</p>
                        </div>
                    </div>
                </li>
                <li id="slide5">
                    <img src="https://lh4.ggpht.com/uRCHOsbkGrvEzP1v4A19g31LXe6uglFb52ewNe7_SWdKSvGqJlyK1Mm_3hlxvJjppZM%3Dh900" alt="" />
                    <div class="description">
                        <input type="checkbox" id="show-description-5"/>
                        <label for="show-description-5" class="show-description-label">I</label>
                        <div class="description-text">
                            <h2>Nadpis</h2>
                            <p>Text</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 25px;">
    </div> 
</div>