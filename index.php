<!DOCTYPE html>
<html>  
  <head>    
    <title>CrazyMine.eu</title>    
    <meta charset="UTF-8">
    <meta name="author" content="Roman Ondráček">
    <meta name="author" content="Jason Skyedge">
    <meta name="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'>  
  </head>  
  <body>    
    <?php include './includes/header.php'; ?>    
    <div class="left-panel">      
      <div class='new'>        
        <h1>Portál byl ukončen</h1>        
        Oznamuji Vám, že byl portál definitívně ukončen, pokusil jsem se ho několikrát obnovit, ale neúspěšně. A asi ho už neobnovím, protože po nástupu na střední školu již nemám skoro žádný volný čas a vytvářím web pro pár projektů.<br>
        Kdybych později znovu spustil svůj herní server, tak by určitě nebyl pro hru Minecraft, protože Mojang zakázal monetizaci serverů a nyní hru vlastní Microsoft.<br>      
        <small>Autor: Roman3349; Přidáno dne: 20. 11. 2014</small>      
      </div>    
    </div>    
    <?php include './includes/footer.php'; ?>  
  </body>
</html>